package com.example.authenticationservice.service;


import com.example.authenticationservice.entity.Token;

public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);

}
