package com.example.authenticationservice.service;


import com.example.authenticationservice.authen.UserPrincipal;
import com.example.authenticationservice.entity.User;

import java.util.List;

public interface UserService {
    User createUser(User user);
    UserPrincipal findByUsername(String username);
    List<User> listUser();
}
