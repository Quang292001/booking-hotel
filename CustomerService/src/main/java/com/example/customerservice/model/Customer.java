package com.example.customerservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter @Setter
public class Customer {
    public static final String SEQUENCE_NAME = "customer_sequence";
    @Id
    private long customer_Id;
    private String name;
    private String email;
    private String phone;
    private String address;
}
