package com.example.customerservice.service;


public interface SequenceGeneratorService {


    public long generateSequence(String seqName);
}
