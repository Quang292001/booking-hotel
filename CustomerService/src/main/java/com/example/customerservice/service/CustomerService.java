package com.example.customerservice.service;

import com.example.customerservice.model.Customer;

public interface CustomerService {

    public Customer addCustomer(Customer customer);
    public Customer getCustomer(Long idCustomer);
    public void deleteCustomer(Long idCustomer);
}
