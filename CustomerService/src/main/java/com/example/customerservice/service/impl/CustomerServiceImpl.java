package com.example.customerservice.service.impl;

import com.example.customerservice.model.Customer;
import com.example.customerservice.repository.CustomerRepository;
import com.example.customerservice.service.CustomerService;
import com.example.customerservice.service.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;
    @Autowired
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl( CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer addCustomer(Customer customer) {
        customer.setCustomer_Id(sequenceGeneratorService.generateSequence(Customer.SEQUENCE_NAME));
        return customerRepository.save(customer);
    }

    @Override
    public Customer getCustomer(Long idCustomer) {
        return customerRepository.findById(idCustomer).get();
    }

    @Override
    public void deleteCustomer(Long idCustomer) {
         customerRepository.deleteById(idCustomer);
    }
}
