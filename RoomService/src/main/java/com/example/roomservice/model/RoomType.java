package com.example.roomservice.model;

public enum RoomType {
    SINGLE,
    DOUBLE,
    VIP
}
