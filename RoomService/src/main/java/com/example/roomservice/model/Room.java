package com.example.roomservice.model;


import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;


@Getter @Setter @AllArgsConstructor
@Document(collection = "room")
public class Room {

    @Id
    private String id;
    @Indexed(unique = true)
    private Long roomId;
    private BigDecimal roomPrice;
    @Enumerated(EnumType.STRING)
    private RoomType roomType;
    @Enumerated(EnumType.STRING)
    private RoomStatus status;

}
