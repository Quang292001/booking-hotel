package com.example.roomservice.repository;


import com.example.roomservice.model.Room;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends MongoRepository<Room,Long> {
    void deleteById(long idInventory);
    public List<Room> findAll();


    public Room findByRoomId(long id);
}
