package com.example.roomservice.service;


import com.example.roomservice.model.Room;

import java.time.LocalDate;
import java.util.List;

public interface RoomService {

    //kiểm tra xem có kho phòng trống không
    public List<Room> getAvailableRooms(String roomType);
    //Thêm kho phòng
    public Room addRoom(Room room);
    public List<Room> lisRooms();
    public void deleteRoom(long idRoom);
    public Room updateRoom(Room room);
    public Room putRoom(Long roomId) throws Exception;
    //Trả p trống
    public Room rebackRoom(Long roomId) throws Exception;
    public Room findById(Long id);

}
