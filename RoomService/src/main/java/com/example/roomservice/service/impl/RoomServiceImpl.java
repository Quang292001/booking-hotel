package com.example.roomservice.service.impl;


import com.example.roomservice.model.Room;
import com.example.roomservice.model.RoomStatus;
import com.example.roomservice.repository.RoomRepository;
import com.example.roomservice.service.RoomService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private final MongoTemplate mongoTemplate;

    @Autowired
    private final RoomRepository roomRepository;

    public RoomServiceImpl(MongoTemplate mongoTemplate, RoomRepository roomRepository) {
        this.mongoTemplate = mongoTemplate;
        this.roomRepository = roomRepository;
    }


    //Lấy thông tin tất cả kho phòng còn trống
    @Override
    public List<Room> getAvailableRooms( String roomType) {
// Create a query object
        Query query = new Query();
        // Set query criteria using operators
        query.addCriteria(Criteria.where("roomType").is(roomType));
//        query.addCriteria(Criteria.where("checkInDate").gte(checkInDate));
//        query.addCriteria(Criteria.where("checkOutDate").lte(checkOutDate));
        query.addCriteria(Criteria.where("status").is(RoomStatus.AVAILABLE));

        return mongoTemplate.find(query, Room.class);

    }

    @SneakyThrows
    @Override
    public Room addRoom(Room room) {
        // Kiểm tra xem phòng đã tồn tại hay chưa
        Room existingRoom = roomRepository.findById(room.getRoomId()).orElse(null);
        if (existingRoom != null) {
            throw new Exception("Phòng đã tồn tại với ID: " + room.getRoomId());
        }
        // Lưu trữ thông tin phòng vào database
        return roomRepository.save(room);
    }

    @Override
    public List<Room> lisRooms() {
        return roomRepository.findAll();
    }


    @Override
    public void deleteRoom(long idRoom) {
         roomRepository.deleteById(idRoom);
    }

    @SneakyThrows
    public Room updateRoom(Room inventory) {
        // Tìm kiếm phòng theo inventoryId
        Room existingRoom = roomRepository.findById(inventory.getRoomId()).orElse(null);
        if (existingRoom == null) {
            throw new Exception("Phòng không tồn tại với ID: " + inventory.getRoomId());
        }

        // Cập nhật thông tin phòng
        existingRoom.setRoomType(inventory.getRoomType());
        existingRoom.setRoomPrice(inventory.getRoomPrice());
        existingRoom.setStatus(inventory.getStatus());

        // Lưu trữ thông tin phòng đã cập nhật vào database
        return roomRepository.save(existingRoom);
    }
                             //đặt phòng
    @Override
    public Room putRoom(Long roomId) throws Exception {
        Room existingRoom = roomRepository.findByRoomId(roomId);

        System.out.println(existingRoom.getRoomId());
        if (existingRoom == null) {
            throw new Exception("Phòng không tồn tại với ID: " + roomId);
        }
        Long getIdroom= existingRoom.getRoomId();
        // Cập nhật trạng thái phòng từ AVAILABLE sang OCCUPIED
        if (existingRoom.getStatus() == RoomStatus.OCCUPIED || existingRoom.getStatus() == RoomStatus.MAINTENANCE ) {
            throw new Exception("Phòng không khả dụng để đặt");
        } else {
            System.out.println(existingRoom.getStatus());
            existingRoom.setStatus(RoomStatus.OCCUPIED);
            System.out.println(existingRoom.getStatus());
            return roomRepository.save(existingRoom);
        }

    }

                    // trả phòng
    @Override
    public Room rebackRoom(Long roomId) throws Exception {
        Room existingRoom = roomRepository.findByRoomId(roomId);

        System.out.println(existingRoom.getRoomId());
        if (existingRoom == null) {
            throw new Exception("Phòng không tồn tại với ID: " + roomId);
        }
        Long getIdroom= existingRoom.getRoomId();
        // Cập nhật trạng thái phòng từ OCCUPIED sang AVAILABLE
        if (existingRoom.getStatus() == RoomStatus.AVAILABLE || existingRoom.getStatus() == RoomStatus.MAINTENANCE ) {
            throw new Exception("Phòng không khả dụng để trả");
        } else {
            System.out.println(existingRoom.getStatus());
            existingRoom.setStatus(RoomStatus.AVAILABLE);
            System.out.println(existingRoom.getStatus());
            return roomRepository.save(existingRoom);
        }
    }
    @Override
    public Room findById(Long id) {
        return roomRepository.findByRoomId(id);
    }


}
