package com.example.roomservice.controller;


import com.example.roomservice.model.Room;
import com.example.roomservice.service.RoomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {

    private final RoomService roomService;
    private static final Logger logger = LoggerFactory.getLogger(RoomController.class);

    public RoomController(RoomService roomService) {
        this.roomService = roomService;
    }


    @GetMapping("/available-rooms")
    public List<Room> getAvailableRooms(
//            @RequestParam("checkInDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkInDate,
//            @RequestParam("checkOutDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkOutDate,
            @RequestParam("roomType") String roomType) {
        return  roomService.getAvailableRooms( roomType);

    }
    @PostMapping
    public Room addRoom(@RequestBody Room room) {
        return roomService.addRoom(room);
    }

    @GetMapping("/list")
    public List<Room> roomList(){
        return roomService.lisRooms();
    }

    @PutMapping("/update/{roomId}")
    public Room updateRoom(@PathVariable Long roomId, @RequestBody Room inventory) {
        inventory.setRoomId(roomId); // Set roomId from path variable
        return roomService.updateRoom(inventory);
    }
    @DeleteMapping("/delete/{roomId}")
    public void deleteRoom(@PathVariable Long roomId){
        roomService.deleteRoom(roomId);
    }

    //đặt phòng
    @PutMapping("/put/{roomId}")
    public Room putRoom(@PathVariable Long roomId) throws Exception {
        // Set roomId from path variable
        return roomService.putRoom(roomId);
    }
    @PostMapping("/back/{roomId}")
    public Room backRoom(@PathVariable Long roomId) throws Exception {
        return  roomService.rebackRoom(roomId);
    }
    @GetMapping("/find/{roomId}")
    public Room findRoom(@PathVariable Long roomId){
        return roomService.findById(roomId);
    }

}
