package com.example.bookingservice.controller;

import com.example.bookingservice.model.Booking;
import com.example.bookingservice.model.Room;
import com.example.bookingservice.service.BookingService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private final BookingService bookingService;

    public Booking fallBackBooking(Throwable throwable) {
        System.out.println("This is a fallback for booking service. Reason: " + throwable.getMessage());
        return null;
    }
    public List<Room> retryFallback(String roomType, Throwable throwable) {
        System.out.println("Retry failed due to: " + throwable.getMessage());
        return List.of(); // or some default response
    }

    public List<Booking> rateLimiterFallback(Throwable throwable) {
        System.out.println("Rate Limiter applied, no further calls are accepted. Reason: " + throwable.getMessage());
        return List.of(); // or some default response
    }

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping("/list")
    @RateLimiter(name="BookingService", fallbackMethod = "rateLimiterFallback")
    public List<Booking> getAllBookings() {
        return bookingService.getAllBookings();
    }

    @GetMapping("/get/{id}")
    public Booking getBookingById(@PathVariable Long id) {
        return bookingService.getBookingbyId(id);
    }

    @PostMapping("/{roomnumber}/{customerId}")
    @CircuitBreaker(name="BookingService", fallbackMethod = "fallBackBooking")
    public Booking createBooking(@PathVariable Long roomnumber,@RequestBody Booking booking,@PathVariable Long customerId) throws Exception {
        return bookingService.bookRoom(roomnumber,booking,customerId);
    }

    @PutMapping("/{id}")
    public Booking updateBooking(@PathVariable Long id, @RequestBody Booking booking) {
        booking.setBooking_Id(id);
        return bookingService.updateBooking(booking);
    }

    //xóa phòng
    @DeleteMapping("/{id}/{roomnumber}/{customerId}")
    public void deleteBooking(@PathVariable Long id,@PathVariable Long roomnumber,@PathVariable Long customerId) {
        bookingService.deleteBooking(id,roomnumber,customerId);
    }

    // kiểm tra phòng có trống không

    private int count = 1;
    @GetMapping("/available-rooms")
    @Retry(name = "BookingService",fallbackMethod = "retryFallback")
    public List<Room> checkAvailableRoom(@RequestParam String roomType) {
        System.out.println("Booking retry call to room" + count ++ +"time at " + new Date());
        return bookingService.checkAvailableRoom(roomType);
    }
}
