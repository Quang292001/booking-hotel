package com.example.bookingservice.service;

import com.example.bookingservice.model.Booking;
import com.example.bookingservice.model.Room;

import java.util.Date;
import java.util.List;

public interface BookingService {

    public List<Booking> getAllBookings();
    public Booking bookRoom(Long roomnumber,Booking booking,Long idCustomer) throws Exception;
    public void deleteBooking(long idBooking,Long roomnumber,Long customerId);
    public Booking updateBooking(Booking booking);
    public Booking getBookingbyId(long idBooking);
    public List<Room> checkAvailableRoom(String roomType);


}
