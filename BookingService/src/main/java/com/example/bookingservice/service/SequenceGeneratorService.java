package com.example.bookingservice.service;

public interface SequenceGeneratorService {
    public long generateSequenceBooking(String seqName);
}
