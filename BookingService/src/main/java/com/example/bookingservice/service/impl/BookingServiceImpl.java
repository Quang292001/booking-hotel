package com.example.bookingservice.service.impl;

import com.example.bookingservice.model.Booking;
import com.example.bookingservice.model.Customer;
import com.example.bookingservice.model.Room;
import com.example.bookingservice.model.RoomStatus;
import com.example.bookingservice.repository.BookingRepository;
import com.example.bookingservice.service.BookingService;
import com.example.bookingservice.service.SequenceGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private final RestTemplate restTemplate;
    @Autowired
    private SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private final BookingRepository bookingRepository;

    public BookingServiceImpl(RestTemplate restTemplate, BookingRepository bookingRepository) {
        this.restTemplate = restTemplate;
        this.bookingRepository = bookingRepository;
    }

    @Override
    public List<Booking> getAllBookings() {
        return bookingRepository.findAll();
    }

    //đặt phòng
    @Override
    public Booking bookRoom(Long roomnumber,Booking booking,Long idCustomer) throws Exception {
        //tạo id tự động cho booking
        booking.setBooking_Id(sequenceGeneratorService.generateSequenceBooking(Booking.SEQUENCE_NAME));
        //truy cập url tới room để thay đổi trạng thái phòng
        String roomServiceUrl = "http://localhost:8000/room/put/" + roomnumber;
        // Tạo request entity với trạng thái mới
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> statusUpdate = new HashMap<>();
        statusUpdate.put("status", RoomStatus.OCCUPIED.name());
        HttpEntity<Map<String, String>> requestEntity = new HttpEntity<>(statusUpdate, headers);

        // Gửi PUT request và nhận lại response
        ResponseEntity<Room> responseEntity = restTemplate.exchange(roomServiceUrl, HttpMethod.PUT, requestEntity, Room.class);
        Room room = responseEntity.getBody();

        // gán id phòng thành số phòng trong booking
        booking.setRoomNumber(room.getRoomId());
        if (room == null || room.getStatus() != RoomStatus.OCCUPIED) {
            throw new Exception("Không thể đặt phòng với ID: " + roomnumber);
        }

        // Lấy thông tin khách hàng từ customer service thông qua id
        String customerServiceUrl = "http://localhost:8000/customer/" + idCustomer;
        Customer customer;
        try {
            customer = restTemplate.getForObject(customerServiceUrl, Customer.class);
            if (customer == null) {
                throw new Exception("Customer không tồn tại với ID: " + idCustomer);
            }
        } catch (RestClientException e) {
            throw new Exception("Lỗi khi lấy thông tin khách hàng: " + e.getMessage());
        }
        booking.setCustomer(customer);
        // Đặt giá trị bookingDate là ngày hiện tại
        booking.setBookingDate(LocalDate.now());
        return bookingRepository.save(booking);
    }
        // Trả phòng
    @Override
    public void deleteBooking(long idBooking,Long roomnumber,Long customerId) {
        //tìm số phòng và xóa
    Room room=restTemplate.postForObject("http://localhost:8084/room/back/"+roomnumber,null,Room.class);
    // tìm thông tin khách hàng và xóa

        // Xử lý kết quả trả về nếu cần (ví dụ: kiểm tra trạng thái của phòng)
        if (room != null && room.getStatus() == RoomStatus.AVAILABLE ) {
            // Xóa booking nếu trả phòng thành công
            bookingRepository.deleteById(idBooking);
            // Gửi yêu cầu DELETE để xóa thông tin khách hàng
            try {
                restTemplate.delete("http://localhost:8000/customer/delete/"+customerId);
            } catch (Exception e) {
                // Nếu xóa khách hàng không thành công, ghi log hoặc xử lý lỗi tùy ý
                throw new IllegalStateException("Không thể kết nối tới dịch vụ khách hàng: " + e.getMessage(), e);
            }
        } else {
            // Xử lý trường hợp không trả phòng thành công
            throw new IllegalStateException("Không thể trả phòng với ID: " + roomnumber);
        }
    }

    @Override
    public Booking updateBooking(Booking booking) {
        // Implement update logic (consider optimistic locking for concurrency control)
        return bookingRepository.save(booking);
    }

    @Override
    public Booking getBookingbyId(long idBooking) {
        return bookingRepository.findById(idBooking).orElse(null);
    }

    //kểm tra có phòng trống không
    @Override
    public List<Room> checkAvailableRoom(String roomType) {
        Room[] rooms=restTemplate.getForObject("http://localhost:8000/room/available-rooms?roomType="+roomType, Room[].class);
        return rooms != null ? Arrays.asList(rooms) : List.of();
    }

}
