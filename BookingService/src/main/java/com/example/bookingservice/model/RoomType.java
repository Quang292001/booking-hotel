package com.example.bookingservice.model;

public enum RoomType {
    SINGLE,
    DOUBLE,
    VIP
}
