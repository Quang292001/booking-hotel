package com.example.bookingservice.model;

import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter @Getter
@Document(collection = "database_sequences_booking")
public class DatabaseSequence {
    @Id
    private String id;
    private long seq;
}
