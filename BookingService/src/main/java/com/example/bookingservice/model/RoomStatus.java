package com.example.bookingservice.model;

public enum RoomStatus {
    AVAILABLE, //Phòng trống, sẵn sàng cho booking.
    OCCUPIED, //Phòng đã được đặt và đang được sử dụng.
    MAINTENANCE, //Phòng đang trong bảo trì và không thể sử dụng.
}
