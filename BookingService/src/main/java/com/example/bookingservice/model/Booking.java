package com.example.bookingservice.model;

import jakarta.persistence.GeneratedValue;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.Date;

@Getter @Setter
public class Booking {
    public static final String SEQUENCE_NAME = "booking_sequence";
    @Id
    private long booking_Id;
    private Long roomNumber;
    private LocalDate bookingDate;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private long total;

    private Customer customer;

}
