package com.example.bookingservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Customer {
    private long customer_Id;
    private String name;
    private String email;
    private String phone;
    private String address;
}
