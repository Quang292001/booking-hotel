package com.example.inventoryservice.controller;

import com.example.inventoryservice.model.Inventory;
import com.example.inventoryservice.service.InventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

    private final InventoryService inventoryService;
    private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping("/available-inventories")
    public List<Inventory> getAvailableRooms(
            @RequestParam("checkInDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkInDate,
            @RequestParam("checkOutDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkOutDate,
            @RequestParam("roomType") String roomType) {
        return  inventoryService.getAvailableInventories(checkInDate, checkOutDate, roomType);

    }
    @PostMapping
    public Inventory addInventory(@RequestBody Inventory inventory) {
        return inventoryService.addInventory(inventory);
    }

    @GetMapping("/list")
    public List<Inventory> inventoryList(){
        return inventoryService.lisInventories();
    }

    @PutMapping("/update/{inventoryId}")
    public Inventory updateInventory(@PathVariable Long inventoryId, @RequestBody Inventory inventory) {
        inventory.setInventoryId(inventoryId); // Set inventoryId from path variable
        return inventoryService.updateInventory(inventory);
    }
    @DeleteMapping("/delete/{inventoryId}")
    public void deleteInventory(@PathVariable Long inventoryId){
        inventoryService.deleteInventory(inventoryId);
    }

    @PostMapping("/post/{inventoryId}")
    public Inventory postInventory(@PathVariable Long inventoryId, @RequestBody Inventory inventory) throws Exception {
        inventory.setInventoryId(inventoryId); // Set inventoryId from path variable
        return inventoryService.postInventory(inventory);
    }

}
