package com.example.inventoryservice.repository;

import com.example.inventoryservice.model.Inventory;
import com.example.inventoryservice.model.InventoryStatus;
import com.example.inventoryservice.model.RoomType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface InventoryRepository extends MongoRepository<Inventory,Long> {
    void deleteById(long idInventory);
    public List<Inventory> findAll();


}
