package com.example.inventoryservice.service;

import com.example.inventoryservice.model.Inventory;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface InventoryService {

    //kiểm tra xem có kho phòng trống không
    public List<Inventory> getAvailableInventories(LocalDate checkInDate, LocalDate checkOutDate, String roomType);
    //Thêm kho phòng
    public Inventory addInventory(Inventory inventory);
    public List<Inventory> lisInventories();
    public void deleteInventory(long idInventory);
    public Inventory updateInventory(Inventory inventory);
    public Inventory postInventory(Inventory inventory) throws Exception;
    //Trả kho phòng trống
    public Inventory rebackInventory(String roomType);

}
