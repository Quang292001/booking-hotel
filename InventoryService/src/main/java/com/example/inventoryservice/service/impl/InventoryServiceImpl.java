package com.example.inventoryservice.service.impl;

import com.example.inventoryservice.model.Inventory;
import com.example.inventoryservice.model.InventoryStatus;
import com.example.inventoryservice.model.RoomType;
import com.example.inventoryservice.repository.InventoryRepository;
import com.example.inventoryservice.service.InventoryService;
import lombok.SneakyThrows;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private final MongoTemplate mongoTemplate;

    @Autowired
    private final InventoryRepository inventoryRepository;


    public InventoryServiceImpl(MongoTemplate mongoTemplate, InventoryRepository inventoryRepository) {
        this.mongoTemplate = mongoTemplate;
        this.inventoryRepository = inventoryRepository;

    }

    //Lấy thông tin tất cả kho phòng còn trống
    @Override
    public List<Inventory> getAvailableInventories(LocalDate checkInDate, LocalDate checkOutDate, String roomType) {
// Create a query object
        Query query = new Query();
        // Set query criteria using operators
        query.addCriteria(Criteria.where("roomType").is(roomType));
        query.addCriteria(Criteria.where("checkInDate").gte(checkInDate));
        query.addCriteria(Criteria.where("checkOutDate").lte(checkOutDate));
        query.addCriteria(Criteria.where("status").is(InventoryStatus.AVAILABLE));

        return mongoTemplate.find(query, Inventory.class);

    }

    @SneakyThrows
    @Override
    public Inventory addInventory(Inventory inventory) {
        // Kiểm tra xem phòng đã tồn tại hay chưa
        Inventory existingInventory = inventoryRepository.findById(inventory.getInventoryId()).orElse(null);
        if (existingInventory != null) {
            throw new Exception("Phòng đã tồn tại với ID: " + inventory.getInventoryId());
        }
        // Lưu trữ thông tin phòng vào database
        return inventoryRepository.save(inventory);
    }

    @Override
    public List<Inventory> lisInventories() {
        return inventoryRepository.findAll();
    }

    @Override
    public void deleteInventory(long idInventory) {
         inventoryRepository.deleteById(idInventory);
    }

    @SneakyThrows
    public Inventory updateInventory(Inventory inventory) {
        // Tìm kiếm phòng theo inventoryId
        Inventory existingInventory = inventoryRepository.findById(inventory.getInventoryId()).orElse(null);
        if (existingInventory == null) {
            throw new Exception("Phòng không tồn tại với ID: " + inventory.getInventoryId());
        }

        // Cập nhật thông tin phòng
        existingInventory.setRoomType(inventory.getRoomType());
        existingInventory.setPrice(inventory.getPrice());
        existingInventory.setStatus(inventory.getStatus());

        // Lưu trữ thông tin phòng đã cập nhật vào database
        return inventoryRepository.save(existingInventory);
    }
    //đặt kho phòng
    @Override
    public Inventory postInventory(Inventory inventory) throws Exception {
        Inventory existingInventory = inventoryRepository.findById(inventory.getInventoryId()).orElse(null);
        if (existingInventory == null) {
            throw new Exception("Phòng không tồn tại với ID: " + inventory.getInventoryId());
        }
        existingInventory.setStatus(InventoryStatus.OCCUPIED);
        existingInventory.setPrice(inventory.getPrice());
        existingInventory.setRoomType(inventory.getRoomType());
        return  inventoryRepository.save(existingInventory);
    }
// trả kho phòng
    @Override
    public Inventory rebackInventory(String roomType) {

        return null;
    }

}
