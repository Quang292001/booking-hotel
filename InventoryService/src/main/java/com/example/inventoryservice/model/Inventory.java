package com.example.inventoryservice.model;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;


@Getter @Setter
public class Inventory {
    private long inventoryId;
    @Enumerated(EnumType.STRING)
    private RoomType roomType;
//    private LocalDate checkInDate;
//    private LocalDate checkOutDate;
    private double price;
    @Enumerated(EnumType.STRING)
    private InventoryStatus status;
}
