package com.example.inventoryservice.model;

public enum InventoryStatus {
    AVAILABLE, //Phòng trống, sẵn sàng cho booking.
    OCCUPIED, //Phòng đã được đặt và đang được sử dụng.
    MAINTENANCE, //Phòng đang trong bảo trì và không thể sử dụng.
}
