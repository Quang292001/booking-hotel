package com.example.inventoryservice.model;

public enum RoomType {
    SINGLE,
    DOUBLE,
    VIP
}
